<?php
$loader='
<div class="loader">
	<div class="load_body">
		<div class="load_content">
			<div class="loader_spin">							
			</div>
		</div>
	</div>
</div>
';
$header ='<header class="header">
	<div class="outer-container">
		<div class="h-flex">
			<div class="h-col-l">
				<a href="/" class="logo">logo</a>
			</div>
			<div class="h-col-c">
				<nav class="nav">
					<div class="nav-inner">
						<ul class="nav-list custom">
							<li><a href="/company.php">О КОМПАНИИ</a></li>
							<li><a href="/catalog.php">КАТАЛОГ</a></li>
							<li><a href="/manufacturer.php">ПРОИЗВОДИТЕЛЬ</a></li>
							<li><a href="/delivery.php">ДОСТАВКА И ОПЛАТА</a></li>
							<li><a href="/contacts.php">КОНТАКТЫ</a></li>
						</ul>
					</div>
				</nav>
			</div>
			<div class="h-col-r">
				<a style= "cursor: pointer;" id="zapros" class="btn ">Запрос детали</a>
				<div class="h-ctc">
					<a href="tel:+79993006020" class="h-tel">+7(999)300-60-20</a>
					<a href="mailto:example@mail.ru" class="h-email">example@mail.ru</a>
				</div>
				<button class="nav-btn">
					<span>
					</span>
				</button>
			</div>
		</div>
	</div>
</header>'; 

$footer = '<div class="footer">
				<div class="outer-container">
					<div class="footer-block">
						<div class="f-col-l">
							<a href="/" class="logo">logo</a>
						</div>
						<div class="f-nav-inner">
							<ul class="f-nav-list">
								<li class="footer-links"><a href="/company.php">О компании</a></li>
								<li class="footer-links"><a href="/catalog.php">Каталог</a></li>
								<li class="footer-links"><a href="/manufacturer.php">Производитель</a></li>
								<li class="footer-links"><a href="/delivery.php">Доставка и оплата</a></li>
								<li class="footer-links"><a href="/contacts.php">Контакты</a></li>
							</ul>
						</div>
						
						
						<div class="contact">
							<div class="adress">Россия, 64000, Свердловская область, г.Екатеринбург, ул.Гаражная 24</div>
							<a href="tel:+79993006020" class="h-tel">+7(999)300-60-20</a>
							<a href="mailto:example@mail.ru" class="h-email">example@mail.ru</a>
						</div>
					</div>
				</div>
			</div>';

$fancybox = '<div id="fancybox" class="fancybox-container">
	<a class="fancybox__area"></a>
	<div class="fancybox_body">
		<div class="fancybox_content">
			<a class="fancybox__close"><img src="/icons/close_black_48dp.svg" alt=""></a>
				<div class="container">
						<div class=" fancybox-h"><h2>Заказ детали</h2></div>
						<div action="#" class="form validation" enctype="multipart/form-data" method="POST" novalidate id="mainForm">
							<div class="fr-item">
								<label class="input">
									<input type="text" data-parsley-errors-messages-disabled required value name="name" id="name" placeholder="Имя">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="text" value name="company" placeholder="Компания" id="company">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="tel" data-parsley-pattern="\+[0-9]\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}" data-parsley-errors-messages-disabled="" required="" value="" name="phone" placeholder="Телефон" id="phone">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="email" data-parsley-errors-messages-disabled="" required="" value="" name="email" placeholder="E-mail" id="email">
								</label>
							</div>
							<div class="fr-item full-width">
								<label class="input">
										<input type="text" data-parsley-errors-messages-disabled="" required="" value="" name="product" placeholder="Что ищете?" id="product">	
								</label>
							</div>
							<div class="fr-item value-changer">
								<div class="value-changer-title">
									Кол-во:
								</div>
								<div class="value-changer-box">
									<button class="value-changer-sign value-changer-munis">-</button>
									<input  type="text" value="1" id="number_of_sparts" class="value-changer-number vlaue-changer-spars">
									<button class="value-changer-sign value-changer-plus">+</button>
								</div>
							</div>
							<div class="fr-item full-width">
								<label class="textarea">
										<textarea name="comment" placeholder="Комментарий" id="comment"></textarea>	
								</label>
							</div>
							<input type="submit" id="sendMail" class="btn btn-style2 fancybox_btn" value="Оставить заявку" name="button" data-action="p_detail"><div id="errorMess"></div>
						</div>
				</div>
		</div>
	</div>
</div>';

$fancybox_spare = '<div id="fancybox" class="fancybox-container">
	<a class="fancybox__area"></a>
	<div class="fancybox_body">
		<div class="fancybox_content">
			<a class="fancybox__close"><img src="/icons/close_black_48dp.svg" alt=""></a>
				<div class="container">
						<div class=" fancybox-h"><h2>Заказ детали</h2></div>
						<div action="#" class="form validation" enctype="multipart/form-data" method="POST" novalidate id="mainForm">
							<div class="fr-item">
								<label class="input">
									<input type="text" data-parsley-errors-messages-disabled required value name="name" id="name" placeholder="Имя">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="text" value name="company" placeholder="Компания" id="company">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="tel" data-parsley-pattern="\+[0-9]\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}" data-parsley-errors-messages-disabled="" required="" value="" name="phone" placeholder="Телефон" id="phone">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="email" data-parsley-errors-messages-disabled="" required="" value="" name="email" placeholder="E-mail" id="email">
								</label>
							</div>
							<div class="fr-item full-width">
								<label class="input">
										<input type="text" data-parsley-errors-messages-disabled="" required="" value="" name="product" placeholder="Что ищете?" id="product">	
								</label>
							</div>
							<div class="fr-item value-changer">
								<div class="value-changer-title">
									Кол-во:
								</div>
								<div class="value-changer-box">
									<button class="value-changer-sign value-changer-munis">-</button>
									<input  type="text" value="1" id="number_of_sparts" class="value-changer-number vlaue-changer-spars">
									<button class="value-changer-sign value-changer-plus">+</button>
								</div>
							</div>
							<div class="fr-item full-width">
								<label class="textarea">
										<textarea name="comment" placeholder="Комментарий" id="comment"></textarea>	
								</label>
							</div>
							<input type="submit" id="sendMail" class="btn btn-style2 fancybox_btn" value="Оставить заявку" name="button" data-action="p_detail"><div id="errorMess"></div>

						</div>
				</div>
			
		</div>
		<div class="fancybox_img_open">
			<a class="fancybox_img__close"><img src="/icons/close_black_48dp.svg" alt=""></a>
		</div>
	</div>
</div>';

$header_scirpts = '
<script type="text/javascript" src="/scripts/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.lazy.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.lazy.iframe.min.js"></script>
<script type="text/javascript" src="/scripts/lazy-load.js"></script>
<script type="text/javascript" src="/scripts/document.ready.js"></script>
';

$scripts = '
<script type="text/javascript" src="/scripts/arrow_list.js"></script>
<script type="text/javascript" src="/scripts/ct-sub-hidder.js"></script>
<script type="text/javascript" src="/scripts/switching_page.js"></script>
<script type="text/javascript" src="/scripts/detali_zakaz.js"></script>
<script type="text/javascript" src="/scripts/element-search.js"></script>
<script type="text/javascript" src="/scripts/form-mail.js"></script>
<script type="text/javascript" src="/scripts/heade_rscroll.js"></script>
<script type="text/javascript" src="/scripts/nav-btn.js"></script>	
<script type="text/javascript" src="/scripts/img_open.js"></script>
<script type="text/javascript" src="/scripts/slick.min.js"></script>
<script type="text/javascript" src="/scripts/slider_img.js"></script>
<script type="text/javascript" src="/scripts/pagination_list.js"></script>

';
$scripts_search= '

<script type="text/javascript" src="/scripts/arrow_list.js"></script>
<script type="text/javascript" src="/scripts/ct-sub-hidder.js"></script>
<script type="text/javascript" src="/scripts/switching_page.js"></script>
<script type="text/javascript" src="/scripts/detali_zakaz.js"></script>
<script type="text/javascript" src="/scripts/element-search.js"></script>
<script type="text/javascript" src="/scripts/form-mail.js"></script>
<script type="text/javascript" src="/scripts/heade_rscroll.js"></script>	
<script type="text/javascript" src="/scripts/img_open.js"></script>
<script type="text/javascript" src="/scripts/nav-btn.js"></script>	
<script type="text/javascript" src="/scripts/slick.min.js"></script>
<script type="text/javascript" src="/scripts/slider_img.js"></script>
<script type="text/javascript" src="/scripts/pagination_list.js"></script>

';
