$('#sendMail').on('click', function(){
	$('#errorMess').css('color','red');
	var name = $('#name').val();
	var company = $('#company').val().trim();
	var phone = $('#phone').val().trim();
	var email = $('#email').val().trim();
	var product = $('#product').val().trim();
	var number_of_sparts = $('#number_of_sparts').val();
	var comment = $('#comment').val().trim();

	if(name == ""){
		$('#errorMess').text('Введите ваше имя');
		return false;
	}else if(phone == ""){
		$('#errorMess').text('Введите ваш телефон');
		return false;
	}else if(email == "" && email.includes('@')){
		$('#errorMess').text('Введите ваш E-mail');
		return false;
	}else if(!email.includes('@')){
		$('#errorMess').text('Введите правильно E-mail');
		return false;
	}else if(product == ""){
		$('#errorMess').text('Введите что вы ищите');
		return false;
	}
	$('#errorMess').text('');

	$.ajax({
		url: '/feedback.php',
		type: 'POST',
		cache: false,
		data: { 'name': name, 'company': company, 'phone': phone, 'email': email, 'product': product,'number_of_sparts': number_of_sparts, 'comment': comment },
		dataType: 'html',
		beforeSend: function(){
			$('#sendMail').prop('disable', true);
		},
		success: function(data){
			if(!data)
				alert("Сообщение не отправлено");

			else
				$('#errorMess').text('Отправлено').css('color','#FFCE0F');
				$('#mainForm').trigger('reset');
			
			$('#sendMail').prop('disable', false);
		}
	});
});