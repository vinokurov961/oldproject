$('.nav-btn').on('click', function(){
	$(this).toggleClass('active');
	if($(this).hasClass('active')){
		$('.h-col-c').addClass('active');
		$('.h-col-c.active').slideDown(500);

		$('body').css("overflow-y", "hidden");
	}
	if(!$(this).hasClass('active')){
		$('.h-col-c').slideUp(500);
		
		$('.h-col-c').removeClass('active');
		$('body').css("overflow-y", "scroll");

	}
})