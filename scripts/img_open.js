$(document).ready(function(){
	$('.fancybox_img_open').css('opacity', '0');
	$('.fancybox_img_open').css('display', 'none');
	$('.fancybox_img_open').css('visibility', 'hidden');


	$('.s-p-img').on('click', function(e){
		e.preventDefault();

		let img = $(this).find('img');
		img.addClass('fanacybox_img_main');
		$(img).clone().appendTo('.fancybox_img_open');
		$('body').css('overflow-y','hidden');
		$('.fancybox_img_open').css('opacity', '1');
		$('.fancybox_img_open').css('visibility', 'visible');
		$('.fancybox_img_open').css('display', '');
		$('.fancybox-container').css('opacity', '1');
		$('.fancybox-container').css('visibility', 'visible');
		
		$('.fancybox_img__close').on('click', function(e){
			e.preventDefault();
			$('.fancybox_img_open').find('.fanacybox_img_main').remove();
			$('.fancybox_img_open').css('opacity', '0');
			$('.fancybox_img_open').css('display', 'none');
			$('.fancybox_img_open').css('visibility', 'hidden');
			$('.fancybox-container').css('opacity', '0');
			$('.fancybox-container').css('visibility', 'hidden');
			document.body.style.position = '';
			document.body.style.top = '';
			$('body').css('overflow-y','');
		});
		
		$('.fancybox__area').on('click', function(e){
			e.preventDefault();
			$('.fancybox_img_open').find('.fanacybox_img_main').remove();	
			$('.fancybox_img_open').css('opacity', '0');
			$('.fancybox_img_open').css('display', 'none');
			$('.fancybox_img_open').css('visibility', 'hidden');
			$('.fancybox-container').css('opacity', '0');
			$('.fancybox-container').css('visibility', 'hidden');
			document.body.style.position = '';
			document.body.style.top = '';
			$('body').css('overflow-y','');

		});
	});

});