$(document).ready(function() {



	$('.value-changer').css('display', 'none');
	// $('#value-changer').css('opacity ', '0');

	$('.value-changer-munis').on('click',function(){
		
		let value_of_changer = parseInt($('.vlaue-changer-spars').val());
		if(value_of_changer >1){
			value_of_changer = value_of_changer - 1;
			$('.vlaue-changer-spars').val(value_of_changer);
		}
	});
	$('.value-changer-plus').on('click',function(){
		let value_of_changer = parseInt($('.vlaue-changer-spars').val());
		
		value_of_changer = value_of_changer + 1;
		$('.vlaue-changer-spars').val(value_of_changer);
		
	});
	$('.vlaue-changer-spars').on('change',function(){
		let value_of_changer = parseInt($('.vlaue-changer-spars').val());
		$('.vlaue-changer-spars').val(value_of_changer);
		
	});

	$('.fancybox_content').css('opacity', '0');
	$('.fancybox_content').css('display', 'none');
	$('.fancybox_content').css('visibility', 'hidden');

	$('#zapros').on('click', function(e){
		e.preventDefault();
		$('body').css('overflow-y','hidden');
		$('.fancybox_content').css('opacity', '1');
		$('.fancybox_content').css('visibility', 'visible');
		$('.fancybox_content').css('display', '');
		$('.fancybox-container').css('opacity', '1');
		$('.fancybox-container').css('visibility', 'visible');
		$('.fancybox__close').on('click', function(e){
			e.preventDefault();
			$('.fancybox_content').css('opacity', '0');
			$('.fancybox_content').css('visibility', 'hidden');
			$('.fancybox_content').css('display', 'none');
			$('.fancybox-container').css('opacity', '0');
			$('.fancybox-container').css('visibility', 'hidden');

			document.body.style.position = '';
			document.body.style.top = '';
			$('body').css('overflow-y','');
		});
		$('.fancybox__area').on('click', function(e){
			e.preventDefault();
			$('.fancybox_content').css('opacity'	, '0');
			$('.fancybox_content').css('visibility', 'hidden');
			$('.fancybox_content').css('display', 'none');
			$('.fancybox-container').css('opacity', '0');
			$('.fancybox-container').css('visibility', 'hidden');
			document.body.style.position = '';
			document.body.style.top = '';
			$('body').css('overflow-y','');

		});
	});

	$('.order').on('click', function(e){
		e.preventDefault();
		$('.value-changer').css('display', '');
		$('body').css('overflow-y','hidden');
		$('.fancybox_content').css('opacity', '1');
		$('.fancybox_content').css('visibility', 'visible');
		$('.fancybox_content').css('display', '');
		$('.fancybox-container').css('opacity', '1');
		$('.fancybox-container').css('visibility', 'visible');
		let li = $(this).parent('.ctg-zapros').parent('.ctg-box').find('.ctg-caption-list');
		// console.log(li.text());
		li = li.text();
		$('#product').val(li.replace(/\s\s+/g, ' '));
		$('.fancybox__close').on('click', function(e){
			e.preventDefault();
			$('.fancybox_content').css('opacity', '0');
			$('.fancybox_content').css('visibility', 'hidden');
			$('.fancybox_content').css('display', 'none');
			$('.fancybox-container').css('opacity', '0');
			$('.fancybox-container').css('visibility', 'hidden');
			document.body.style.position = '';
			document.body.style.top = '';
			$('body').css('overflow-y','');
			$('.value-changer').css('display', 'none');
		});
		$('.fancybox__area').on('click', function(e){
			e.preventDefault();
			$('.fancybox_content').css('opacity'	, '0');
			$('.fancybox_content').css('visibility', 'hidden');
			$('.fancybox_content').css('display', 'none');
			$('.fancybox-container').css('opacity', '0');
			$('.fancybox-container').css('visibility', 'hidden');
			document.body.style.position = '';
			document.body.style.top = '';
			$('body').css('overflow-y','');
			$('.value-changer').css('display', 'none');
		});
	});
	$('.order_spare').on('click', function(e){
		$('.value-changer').css('display', '');
		e.preventDefault();
		$('body').css('overflow-y','hidden');
		$('.fancybox_content').css('opacity', '1');
		$('.fancybox_content').css('visibility', 'visible');
		$('.fancybox_content').css('display', '');
		$('.fancybox-container').css('opacity', '1');
		$('.fancybox-container').css('visibility', 'visible');
		let li = $(this).parent('.qty-zapros').parent('.s-p-z-qty').parent('.s-p-zapros').parent('.s-p-info').parent('.s-p-main-content').parent('.s-p-main').parent('.spare_part-content').find('h1');
		li = li.text();
		$('#product').val(li.replace(/\s\s+/g, ' '));
		$('.fancybox__close').on('click', function(e){
			e.preventDefault();
			$('.fancybox_content').css('opacity'	, '0');
			$('.fancybox_content').css('visibility', 'hidden');
			$('.fancybox_content').css('display', 'none');
			$('.fancybox-container').css('opacity', '0');
			$('.fancybox-container').css('visibility', 'hidden');
			document.body.style.position = '';
			document.body.style.top = '';
			$('body').css('overflow-y','');
			$('.value-changer').css('display', 'none');
		});
		$('.fancybox__area').on('click', function(e){
			e.preventDefault();
			$('.fancybox_content').css('opacity'	, '0');
			$('.fancybox_content').css('visibility', 'hidden');
			$('.fancybox_content').css('display', 'none');
			$('.fancybox-container').css('opacity', '0');
			$('.fancybox-container').css('visibility', 'hidden');
			document.body.style.position = '';
			document.body.style.top = '';
			$('body').css('overflow-y','');
			$('.value-changer').css('display', 'none');
		});
	});
	
});