$(document).ready(function(){
	$('.slider').slick({
		arrows: true,
		dots: true,
		adaptiveHeight: true,
		slidesToShow: 5,
		speed: 750,
		easing: 'linear',
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		pauseOnFocus: true,
		pauseOnHover: true,
		pauseOnDotsHover: true,
		draggable: true, //для пролистывания с помощью мышки
		swipe: true, //тоже самое только для телефона
		touchThreshold: 5, // сколько нужно перелестнуть чтобы переключился слайдер
		touchMove: true, //чтобы можно было двигать слайд на телефоне
		waitForAnimate: false, //чтобы быстро прокручивать не включая анимацию
		centerMode: true,
		asNavFor:".sliderBig",//связываем два слайдера
		responsive:[ 
			{
				breakpoint: 1423,
				settings:{
					slidesToShow:3,
					touchThreshold:3,
				}
			},
			{
				breakpoint: 650,
				settings:{
					slidesToShow:1,
					touchThreshold:1,
				}
			}
		],
		// mobileFirst:false,
	});

	$('.sliderBig').slick({
		arrows: false,
		// fade: true,
		asNavFor:".slider",

	});
});