<?php 
include('../static.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Error 404</title>
    <link rel="stylesheet" href="/style.css">
    <?php echo $header_scirpts;?>
</head>
<body style="overflow-y: hidden;">
    <div class="site-wrapper">
        <?php echo $loader;?>
        <?php echo $header; ?>
        <main class="content">
            <div class="top-screen__catalog index-screen__catalog" style="background-image: url(/img/contacts.jpg); background-position: center; background-size: cover ; background-size: cover ;">
                <div class="container">
                    <div class="ts-head">
                        <h1>Ошибка страницы</h1>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="err-wrapper">
                    <div class="err-mess">
                        <h1>ОШИБКА 404</h1>
                        <P>К сожалению страница не найдена</P>
                        <a href="/" class="btn">Главная страница</a>
                    </div>
                    <div class="err-img"><img src="/img/error.png" alt=""></div>
                </div>
            </div>
        </main>
        <?php echo $footer; ?>
    </div>
<?php echo $fancybox; ?>
<?php echo $scripts; ?>
</body>
</html>