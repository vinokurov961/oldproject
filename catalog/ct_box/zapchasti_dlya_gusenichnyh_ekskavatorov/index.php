<?php

require_once "../../../db.php";
include("../../../static.php");

$box = $pdo->query("select * from ct_box");
$ct_box = $box->fetchAll();
$zapchasty = $pdo->query("select * from ct_box where id_ct_box = 1");
$zapchasty_caption = $zapchasty->fetchAll();
$sub_link = $pdo->query("select * from ct_sub_link");
$ct_sub_link = $sub_link->fetchAll();
$spare = $pdo->query("select * from spare_part");
$spare_part = $spare->fetchAll();
$zapchasty_link = $pdo->query("select * from ct_sub_link where id_ct_box = 1");
$zapchasty_link_value = $zapchasty_link->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Грот</title>
	<link rel="stylesheet" href="/style.css">
	<?php echo $header_scirpts;?>
</head>
<body style="overflow-y: hidden;">
	<div class="site-wrapper">
		<?php echo $loader;?>
		<?php echo $header; ?>
		<main class="content">
			<div class="top-screen__catalog index-screen__catalog" style="background-image: url(/img/background_catalog.jpg);background-position: center; background-size: cover ;">
  				<div class="container">
  					<div class="ts-seatch">
  						<div class="sr-caption">ПОИСК ПО АРТИКУЛУ	
  						</div>
  						<form action="/search.php" class="h-search" method="GET">
  							<input type="text" placeholder="Введите номер или название запчасти" name="search" id="elem-search">
  							<button id="find-btn" name="find_btn">Найти</button>
  						</form>

  						<ul class="elements-search">
  							<?php foreach($spare_part as $spare_parts): ?>
  								<li value="<?= $spare_parts["id_spare_part"]; ?>">
  									<a href="<?= $spare_parts["spare_part_link"]; ?>">
  										<p>Наименование запчасти:</p> 
  										<div class="title_spare_name">
  											<?= $spare_parts["spare_part_name"]; ?>
  										</div>  
  										<br>
  										<p>Артикул запчасти:</p> 
  										<div class="title_vender">
  											<?= $spare_parts["vender_code"]; ?>
  										</div>
  									</a>
  								</li>
  							<?php endforeach; ?>
  						</ul>
  						<div class="h-sub-search">
  							Введите номер детали или название, например 227-6949
  						</div>

  					</div>

  					</div>
				</div>
			<div class="main-catalog">
				<div class="container">
					<div class="main-cols">
						<div class="aside">
							<div class="sled">
								<div class="as-box">
									<ul class="as-list">
										<?php foreach($ct_box as $ct_boxs): ?>
												<?php if ($ct_boxs["id_ct_box"] ==  1):?>
													<li class="subnav acitve" >
												<?php else: ?>
													<li class="subnav" >
												<?php endif; ?>
													<a href="/<?= $ct_boxs["ct_caption_link"] ?>" class="op-active"><?= $ct_boxs["ct_caption"] ?></a>
													<i class="sub-arrow-r">
														<span class="s-arrow-l"></span>
														<span class="s-arrow-r"></span>
													</i>
													
													<div class="subnav-wrp" style="display: none;">
														<ul class="subnav-list">
															<li >
																<?php foreach($ct_sub_link as $ct_sub_links): ?>
																	<?php if($ct_sub_links["id_ct_box"] ==$ct_boxs["id_ct_box"] ): ?>
																		<a href="/<?= $ct_sub_links["sub_link_link"] ?>"><?= $ct_sub_links["sub_link_text"]?>
																		</a>
																	<?php endif; ?>
																<?php endforeach; ?>
															</li>
														</ul>
													</div>
													
												</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="tc-content">
							<h1>
								<?php foreach($zapchasty_caption as $zapchasty_caption): ?>
									<?= $zapchasty_caption["ct_caption"] ?>
								<?php endforeach; ?>	
							</h1>
								<div class="td-wrp">
									<ul class="td-list">
										<?php foreach($zapchasty_link_value as $zapchasty_link_values): ?>
										<li>
												<a href="/<?= $zapchasty_link_values["sub_link_link"] ?>"><?= $zapchasty_link_values["sub_link_text"]?>
												</a>
											
										</li>
										<?php endforeach; ?>
									</ul>
								</div>

						</div>
					</div>
				</div>
			</div>
			
			<!-- <div class="zakaz-denali" style="background-image: url(../img/background_header.jpg);">
				<div class="container">
					<div class="zakaz-denali-c">
						<div class="zakaz-denali-h"><h2>Заказ детали</h2></div>
						<form action="#" class="form validation" enctype="multipart/form-data" method="POST" novalidate id="mainForm">
							
							<div class="fr-item">
								<label class="input">
									<input type="text" data-parsley-errors-messages-disabled required value name="name" id="name" placeholder="Имя">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="text" value name="company" placeholder="Компания" id="company">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="tel" data-parsley-pattern="\+[0-9]\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}" data-parsley-errors-messages-disabled="" required="" value="" name="phone" placeholder="Телефон" id="phone">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="email" data-parsley-errors-messages-disabled="" required="" value="" name="email" placeholder="E-mail" id="email">
								</label>
							</div>
							<div class="fr-item full-width">
								<label class="input">
									<input type="text" data-parsley-errors-messages-disabled="" required="" value="" name="product" placeholder="Что ищете?" id="product">	
								</label>
							</div>
							<div class="fr-item full-width">
								<label class="textarea">
										<textarea name="comment" placeholder="Комментарий" id="comment"></textarea>
								</label>
							</div>
							 <div class="fr-item full-width">
								<div class="fr-title">Прикрепить файл</div>
								<div class="file-wrp">
									<div class="file dz-clickable">
										Выберите 
									</div>
								</div>
							</div>
							<div class="footer-btn">
								<input type="submit" id="sendMail" class="btn btn-style2" value="Оставить заявку" name="button" data-action="p_detail"><div id="errorMess"></div>
							<div class="errorMess" id="errorMess"></div>
							</div>
							
						</form>
					</div>
				</div>
			</div>   -->
		</main>
		<?php echo $footer; ?>
	</div>
<?php echo $fancybox; ?>
<?php echo $scripts; ?>


</body>
</html>