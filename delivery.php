<?php 
include('static.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Грот</title>
	<link rel="stylesheet" href="style.css">
	<?php echo $header_scirpts;?>
</head>
<body style="overflow-y: hidden;">
	<div class="site-wrapper">
		<?php echo $loader;?>
		<?php echo $header; ?>
		<main class="content">
			<div class="top-screen__catalog index-screen__catalog" style="background-image: url(img/background_contact.jpg); background-position: center; background-size: cover ;">
  				<div class="container">
  					<div class="ts-head">
  						<h1>доставка и оплата</h1>
  					</div>
  				</div>
			</div>

			<div class="container">
				<h1 class="delivery_h1"><p >Доставку запасных частей осуществляем в любую точку России, любыми транспортными компаниями, по предпочтению клиента.</p></h1>
				<br>
				<h1 class="delivery_h1"><p>Применяем все виды доставок</p></h1>
				<ul class="ed-list column">
					<li>
						<div class="img-text">
							<div class="it-img">
								<img  src="icons/local_shipping_black_48dp.svg" alt="">
							</div>
							<span>
								наземные
							</span>
						</div>
					</li>
					<li>
						<div class="img-text">
							<div class="it-img">
								<img src="icons/train_black_48dp.svg" alt="">
							</div>
							<span>
								железнодорожные
							</span>
						</div>
					</li>
					<li>
						<div class="img-text">
							<div class="it-img">
								<img src="icons/flight_black_48dp.svg" alt="">
							</div>
							<span>
								авиа
							</span>
						</div>
					</li>
					<li>
						<div class="img-text">
							<div class="it-img">
								<img src="icons/directions_bike_black_48dp.svg" alt="">
							</div>
							<span>
								курьерские
							</span>
						</div>
					</li>
				</ul>
				<br>
				<p><strong>Оптимальный способ доставки помогут подобрать и просчитать наши менеджеры.</strong></p>
				<ul class="icons-list">
					<li>
						<div class="icon-box">
							<div class="ic-img">
								<img src="icons/download_black_48dp.svg" alt="">
							</div>
							<div class="ic-text">
								<span>
									Доставка до терминала транспортной компании бесплатна для клиента.
								</span>
							</div>
						</div>
					</li>
					<li>
						<div class="icon-box">
							<div class="ic-img">
								<img src="icons/upload_black_48dp.svg" alt="">
							</div>
							<div class="ic-text">
								<span>
									Отгрузка запасных частей осуществляется с основных городов России – Москва, Санкт-Петербург и Екатеринбург.
								</span>
							</div>
						</div>
					</li>
				</ul>
				
				<br>
				<p><strong>Оплата производится по безналичному расчету (расчетный счет) с НДС/ Другие варианты оплаты обговариваются с представителем компании</strong></p>
				<br>
				<p><strong>Так же для наших постоянных клиентов возможна</strong></p>
				<ul class="ed-list_posnoyan ">
					<li>
						<div class="img-text">
							<div class="it-img">
								<img src="icons/savings_black_48dp.svg" alt="">
							</div>
							<span>
								частичная оплата
							</span>
						</div>
					</li>
					<li>
						<div class="img-text">
							<div class="it-img">
								<img src="icons/av_timer_black_48dp.svg" alt="">
							</div>
							<span>
								отсрочка платежа
							</span>
						</div>
					</li>
					<li>
						<div class="img-text">
							<div class="it-img">
								<img src="icons/local_offer_black_48dp.svg" alt="">
							</div>
							<span>
								скидки и различные бонусы (бесплатная доставка, акции и тд.)
							</span>
						</div>
					</li>
				</ul>
				<br>
				<h1 class="delivery_h1"><p>на первый заказ мы даем скидку 5%</p></h1>
			</div>
		</main>
		<?php echo $footer; ?>	
	</div>
<?php echo $fancybox; ?>
<?php echo $scripts; ?>
</body>
</html>