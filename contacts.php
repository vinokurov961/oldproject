<?php 
include('static.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Грот</title>
	<link rel="stylesheet" href="style.css">
	<?php echo $header_scirpts;?>
</head>
<body style="overflow-y: hidden;">
	<div class="site-wrapper">
		<?php echo $loader;?>
		<?php echo $header; ?>
		<main class="content">
			<div class="top-screen__catalog index-screen__catalog" style="background-image: url(img/contacts.jpg); background-position: center; background-size: cover ; background-size: cover ;">
  				<div class="container">
  					<div class="ts-head">
  						<h1>контакты</h1>
  					</div>
  				</div>
			</div>

			<div class="container">
				<h2>ЗВОНИТЕ В ЛЮБОЕ ВРЕМЯ, ВАМ ВСЕГДА ОТВЕТЯТ</h2>
				<div class="cts-flex">
					<div class="cts-item">
						<div class="cts-title">
							Общий телефон
						</div>
						<a href="tel: +79993006020" class="cts-prop">
							<span>+7 (999) 300 60-20</span>
						</a>
					</div>
					<div class="cts-item">
						<div class="cts-title">
							Общая почта
						</div>
						<a href="mailto: example@mail.ru" class="cts-prop">example@mail.ru</a>
					</div>
				</div>
				<h2>Менеджеры</h2>
				<div class="mg-wrp">
					<div class="mg-item">
						<div class="mg-name">
							Винокуров Евгений Алексеевич
						</div>
						<a href="tel: +79993006020" class="mg-tel">+7 (999) 300 60-20</a>
						<a href="mailto: example@mail.ru" class="mg-tel">example@mail.ru</a>
					</div>
					<div class="mg-item">
						<div class="mg-name">
							Винокуров Алексей Николаевич
						</div>
						<a href="tel: +79993006020" class="mg-tel">+7 (999) 300 60-20</a>
						<a href="mailto: example@mail.ru" class="mg-tel">example@mail.ru</a>
					</div>
				</div>
				<div class="ab-m-map">
					<iframe class="lazy" data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2187.2150103281297!2d60.53304741587938!3d56.75644538083372!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43c1661a08079079%3A0xa494ecd45d21bec4!2z0JPQsNGA0LDQttC90LDRjyDRg9C7LiwgMjQsINCV0LrQsNGC0LXRgNC40L3QsdGD0YDQsywg0KHQstC10YDQtNC70L7QstGB0LrQsNGPINC-0LHQuy4sIDYyMDAxNg!5e0!3m2!1sru!2sru!4v1632997326894!5m2!1sru!2sru" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
				</div>
				<div class="addr-wrp">
					<div class="addr-item">
						<div class="addr-content">
							Часы работы: с 10:00 до 19:00 (Пн-Пт) 
							<br>
							с 10:00 до 15:00 (Сб)
							<br>
							Вс - выходной день			
						</div>
					</div>
					<div class="addr-item">
						<div class="addr-content">
							Россия, 64000, Свердловская область, г.Екатеринбург, ул.Гаражная 24
								
						</div>
					</div>
				</div>
			</div>
		</main>
		<?php echo $footer; ?>
	</div>
<?php echo $fancybox; ?>
<?php echo $scripts; ?>
</body>
</html>