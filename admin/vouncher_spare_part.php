<!-- create folder spare_part -->
<?php

require_once "../db.php";




$value_ct_sub = $_POST['value_ct_sub'];
$value_ct_sub_link = $_POST['value_ct_sub_link'];
$value_id_spare_part=$_POST['value_id_spare_part'];

$box_link = $pdo->query("select * from ct_sub_link where id_ct_sub_link  = '$value_ct_sub_link'");
$ct_box_link = $box_link->fetchAll();


$box = $pdo->query("select * from ct_box where id_ct_box  = '$value_ct_sub'");
$ct_box = $box->fetchAll();

$spare = $pdo->query("select * from spare_part where id_spare_part = '$value_id_spare_part'");
$spare_part = $spare->fetchAll();



$strOut = '<?php

require_once "../../../../../db.php";
include("../../../../../static.php");  
		

$box = $pdo->query("select * from ct_box");
$ct_box = $box->fetchAll();
$zapchasty = $pdo->query("select * from ct_sub_link where id_ct_sub_link = '.$value_id_spare_part.'");
$zapchasty_caption = $zapchasty->fetchAll();
$sub_link = $pdo->query("select * from ct_sub_link");
$ct_sub_link = $sub_link->fetchAll();
$spare = $pdo->query("select * from spare_part");
$spare_part = $spare->fetchAll();
$spare_ct_link = $pdo->query("select * from spare_part where id_ct_sub_link = '.$value_ct_sub_link.' and id_spare_part = '.$value_id_spare_part.'");
$spare_part_ct_link = $spare_ct_link->fetchAll();
foreach($spare_part_ct_link as $spare_part_ct_links){
	$val= $spare_part_ct_links["id_manufacturer"];
	$man = $pdo->query("select * from manufacturer where id_manufacturer = $val");
	$manufactur = $man->fetchAll();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Грот</title>
	<link rel="stylesheet" href="/style.css">
	<?php echo $header_scirpts;?>
</head>
<body style="overflow-y: hidden;">

	<div class="site-wrapper">
		<?php echo $loader;?>
		<?php echo $header; ?>
		<main class="content">
			<div class="top-screen__catalog index-screen__catalog" style="background-image: url(/img/background_catalog.jpg); background-position: center; background-size: cover ;">
  				<div class="container">
  					<div class="ts-seatch">
  						<div class="sr-caption">ПОИСК ПО АРТИКУЛУ	
  						</div>
  						<form action="/search.php" class="h-search" method="GET">
  							<input type="text" placeholder="Введите номер или название запчасти" name="search" id="elem-search" autocomplete="off">
  							<button id="find-btn" name="find_btn">Найти</button>
  						</form>

  						<ul class="elements-search">
  							<?php foreach($spare_part as $spare_parts): ?>
  								<li value="<?= $spare_parts["id_spare_part"]; ?>">
  									<a href="/<?= $spare_parts["spare_part_link"]; ?>">
  										<p>Наименование запчасти:</p> 
  										<div class="title_spare_name">
  											<?= $spare_parts["spare_part_name"]; ?>
  										</div>  
  										<br>
  										<p>Артикул запчасти:</p> 
  										<div class="title_vender">
  											<?= $spare_parts["vender_code"]; ?>
  										</div>
  									</a>
  								</li>
  							<?php endforeach; ?>
  						</ul>
  						<div class="h-sub-search">
  							Введите номер детали или название, например 227-6949
  						</div>

  					</div>

  					</div>
				</div>
			<div class="main-catalog">
				<div class="container">
					<div class="main-cols">
						<div class="aside">
							<div class="sled">
								<div class="as-box">
									<ul class="as-list">
										<?php foreach($ct_box as $ct_boxs): ?>
												<?php if ($ct_boxs["id_ct_box"] ==  '.$value_ct_sub.'):?>
													<li class="subnav acitve" >
												<?php else: ?>
													<li class="subnav" >
												<?php endif; ?>
													
													<a href="/<?= $ct_boxs["ct_caption_link"] ?>" class="op-active"><?= $ct_boxs["ct_caption"] ?></a>
													<i class="sub-arrow-r">
														<span class="s-arrow-l"></span>
														<span class="s-arrow-r"></span>
													</i>
													
													<div class="subnav-wrp" style="display: none;">
														<ul class="subnav-list">
															<li >
																<?php foreach($ct_sub_link as $ct_sub_links): ?>
																	<?php if($ct_sub_links["id_ct_box"] ==$ct_boxs["id_ct_box"] ): ?>
																		<?php if ($ct_sub_links["id_ct_sub_link"] ==  '.$value_ct_sub_link.'):?>
																			<a href="/<?= $ct_sub_links["sub_link_link"] ?>"class = "active"><?= $ct_sub_links["sub_link_text"]?>
																			</a>
																		<?php else: ?>
																			<a href="/<?= $ct_sub_links["sub_link_link"] ?>"><?= $ct_sub_links["sub_link_text"]?>
																			</a>
																		<?php endif; ?>
																	<?php endif; ?>
																<?php endforeach; ?>
															</li>
														</ul>
													</div>
													
												</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="spare_part-content">
							<?php foreach($spare_part_ct_link as $spare_part_ct_links): ?>
							<h1>
								<?= $spare_part_ct_links["spare_part_name"] ?>	
							</h1>
							<?php foreach($manufactur as $manufactures): ?>
							<div class="s-p-main">
								<div class="s-p-main-content">
									<div class="s-p-img">
										<img src="img/spare.jpg" alt="">
									</div>
									<div class="s-p-info">
										<ul class="info-list">
											<li>
												<div class="i-l-title">
													Производитель:
												</div>
												
												<div class="i-l-prop">
														<a href="/<?= $manufactures["link_manufacturer"]?>">
														<?= $manufactures["name_manufacturer"]?>
														</a>	
												</div>
												
											</li>
											<hr>
											<li>
												<div class="i-l-title">
													Артикул:
												</div>
												<div class="i-l-prop">
													<?= $spare_part_ct_links["vender_code"] ?>
												</div>
											</li>
											<hr>
											<li>
												<div class="i-l-title">
													Колиество на складе:
												</div>
												<div class="i-l-prop">
													<?= $spare_part_ct_links["number"] ?> шт.
												</div>
											</li>
										</ul>
										<div class="s-p-zapros">
											<div class="s-p-z-qty">
												<div class="z-qty-label">
													Кол-во:
												</div>
												<div class="qty-value">
													<button class="qty-value-changer value-changer-munis">-</button>
													<input class="vlaue-changer-spars" type="text" value="1">
													<button  class="qty-value-changer value-changer-plus">+</button>
												</div>
												<div class="qty-zapros">
													<a  style= "cursor: pointer;" id="order_spare" class="btn order_spare">Заказать</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<p><?= $spare_part_ct_links["comment"] ?></p>
								<hr>
								<p>
									<?= $spare_part_ct_links["spare_part_name"] ?>
									<?= $spare_part_ct_links["vender_code"] ?>
								</p>
								<hr>
								<p>Доставку запасных частей осуществляем в любую точку России, любыми транспортными компаниями, по предпочтению клиента.</p>
								<br>
								<p>Применяем все виды доставок</p>
								<div class="spare-icon-inner">
									<ul class="s-i-list">
									<li>
										<div>
											<div>
												<img  src="/icons/local_shipping_black_48dp.svg" alt="">
											</div>
											<span>
												наземные
											</span>
										</div>
									</li>
									<li>
										<div >
											<div>
												<img src="/icons/train_black_48dp.svg" alt="">
											</div>
											<span>
												железнодорожные
											</span>
										</div>
									</li>
									<li>
										<div >
											<div>
												<img src="/icons/flight_black_48dp.svg" alt="">
											</div>
											<span>
												авиа
											</span>
										</div>
									</li>
									<li>
										<div >
											<div >
												<img src="/icons/directions_bike_black_48dp.svg" alt="">
											</div>
											<span>
												курьерские
											</span>
										</div>
									</li>
								</ul>
								</div>
								<?php endforeach ?>
							</div>	
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
			
			<!-- <div class="zakaz-denali" style="background-image: url(/img/background_header.jpg);">
				<div class="container">
					<div class="zakaz-denali-c">
						<div class="zakaz-denali-h"><h2>Заказ детали</h2></div>
						<form action="#" class="form validation" enctype="multipart/form-data" method="POST" novalidate id="mainForm">
							
							<div class="fr-item">
								<label class="input">
									<input type="text" data-parsley-errors-messages-disabled required value name="name" id="name" placeholder="Имя">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="text" value name="company" placeholder="Компания" id="company">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="tel" data-parsley-pattern="\+[0-9]\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}" data-parsley-errors-messages-disabled="" required="" value="" name="phone" placeholder="Телефон" id="phone">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="email" data-parsley-errors-messages-disabled="" required="" value="" name="email" placeholder="E-mail" id="email">
								</label>
							</div>
							<div class="fr-item full-width">
								<label class="input">
									<input type="text" data-parsley-errors-messages-disabled="" required="" value="" name="product" placeholder="Что ищете?" id="product">	
								</label>
							</div>
							<div class="fr-item full-width">
								<label class="textarea">
										<textarea name="comment" placeholder="Комментарий" id="comment"></textarea>
								</label>
							</div>
							 <div class="fr-item full-width">
								<div class="fr-title">Прикрепить файл</div>
								<div class="file-wrp">
									<div class="file dz-clickable">
										Выберите 
									</div>
								</div>
							</div>
							<div class="footer-btn">
								<input type="submit" id="sendMail" class="btn btn-style2" value="Оставить заявку" name="button" data-action="p_detail"><div id="errorMess"></div>
							<div class="errorMess" id="errorMess"></div>
							</div>
							
						</form>
					</div>
				</div>
			</div>   -->

		</main>

			<?php echo $footer; ?>
	</div>
<?php echo $fancybox_spare; ?>
<?php echo $scripts; ?>


</body>
</html>';

function translit($s) {
	$s = (string) $s; // преобразуем в строковое значение
	$s = trim($s); // убираем пробелы в начале и конце строки
	$s = str_replace(' ', '_', $s);//Замена пробелов символами подчеркивания
	$s = str_replace('/', '-', $s);//Замена слешев символами тире
	$s = str_replace(':', '', $s);//Удаление двоеточия 
	$s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
	$s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
	return $s; // возвращаем результат
}

// foreach($ct_box as $ct_boxs){
// 			$ct_boxs_link = $ct_boxs['ct_caption_link'];
			
// }

if(isset($value_ct_sub_link) and ($value_ct_sub_link != 0) and isset($value_ct_sub) and $value_ct_sub != 0 and isset($value_id_spare_part) and $value_id_spare_part != 0){
	foreach($ct_box as $ct_boxs){
		foreach($ct_box_link as $ct_box_links){
			foreach($spare_part as $spare_parts){
			$caption_spare_part = translit($spare_parts['spare_part_name']);
			$caption_sub_link = translit($ct_box_links['sub_link_text']);
			$caption_ct_caption = translit($ct_boxs['ct_caption']);
			mkdir("../catalog/ct_box/$caption_ct_caption/$caption_sub_link/$caption_spare_part", 0700);
			mkdir("../catalog/ct_box/$caption_ct_caption/$caption_sub_link/$caption_spare_part/img", 0700);
			$img_link_spare = "../catalog/ct_box/$caption_ct_caption/$caption_sub_link/$caption_spare_part/img";
			$f = fopen("../catalog/ct_box/$caption_ct_caption/$caption_sub_link/$caption_spare_part/index.php", "w"); 
			fwrite($f, $strOut); 
			fclose($f); 
			$stmt = $pdo->prepare("update spare_part SET spare_part_link = 'catalog/ct_box/$caption_ct_caption/$caption_sub_link/$caption_spare_part/', img_spare_part ='catalog/ct_box/$caption_ct_caption/$caption_sub_link/$caption_spare_part/img/spare.jpg' where id_spare_part  = '$value_id_spare_part'");   
			$stmt->execute();
			}
		}
	}


}

if(isset($_POST['my_file_upload']) and isset($_POST['value_id_spare_part'])){  
// ВАЖНО! тут должны быть все проверки безопасности передавемых файлов и вывести ошибки если нужно
	// $uploaddir = './uploads'; // . - текущая папка где находится submit.php
	// cоздадим папку если её нет
	// if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
	$files      = $_FILES; // полученные файлы
	// $done_files = array();
	$value_id_spare_part = $_POST['value_id_spare_part'];
	echo $value_id_spare_part;


	// переместим файлы из временной директории в указанную
	foreach( $files as $file ){
		$file_name = $file['name'];

	// 	if( move_uploaded_file( $file['tmp_name'], "$uploaddir/$file_name" ) ){
	// 		$done_files[] = realpath( "$uploaddir/$file_name" );
	// 	}	
			$spare = $pdo->query("select * from spare_part where id_spare_part = '$value_id_spare_part'");
			$spare_part = $spare->fetchAll();
			foreach($spare_part as $spare_parts){
			 $apppath = '../'.$spare_parts['img_spare_part'];
			}
			$uploadfile = $apppath;
			move_uploaded_file($file['tmp_name'], $uploadfile);

		
	}

	// $data = $done_files ? array('files' => $done_files ) : array('error' => 'Ошибка загрузки файлов.');

	// die( json_encode( $data ) );
}