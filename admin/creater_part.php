<?php

require_once "../db.php";

$spare_part_name =$_POST['input_part_name'];
$vender_code =$_POST['input_vender_code'];
$number =$_POST['input_number'];
$comment =$_POST['input_comment'];
$id_manufacturer =$_POST['value_id_man'];
$id_ct_sub_link = $_POST['value_ct_sub_link'];
if(isset($spare_part_name) and isset($vender_code) and isset($number) and isset($comment) and isset($id_manufacturer) and isset($id_ct_sub_link)){
   $stmt = $pdo->prepare("insert into spare_part (spare_part_name, spare_part_link, vender_code, number, comment, img_spare_part, id_ct_sub_link, id_manufacturer) values(?,?,?,?,?,?,?,?)");   
   $stmt->execute([
      $spare_part_name,
      "",
      $vender_code,
      $number,
      $comment,
      "",
      $id_ct_sub_link,
      $id_manufacturer,
   ]);   
}
   
