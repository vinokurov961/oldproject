<?php 
include('static.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Грот</title>
	<link rel="stylesheet" href="style.css">
	<?php echo $header_scirpts;?>
</head>
<body style="overflow-y: hidden;">
	<div class="site-wrapper">
		<?php echo $loader;?>
		<?php echo $header; ?>
		<main class="content">
			<div class="top-screen__catalog index-screen__catalog" style="background-image: url(img/companyjpg.jpg); background-position: center; background-size: cover ;">
  				<div class="container">
  					<div class="ts-head">
  						<h1>О компании</h1>
  					</div>
  				</div>
			</div>

			<div class="container">
				<h2>ДОБРО ПОЖАЛОВАТЬ В ОРГАНИЗАЦИЮ «ГРОТ»</h2>
				<p>Наша компания занимается поставкой запасных частей для Землеройной, Горной и Cтроительной техники — Импортного, Китайского и Отечественного производителя</p>
				<br>
				<ul class="icons-list">
					<li>
						<div class="icon-box">
							<div class="ic-img">
								<img src="icons/local_shipping_black_48dp.svg" alt="">
							</div>
							<div class="ic-text">
								<span>
									Поставка запчастей в любые регионы России осуществляется в кратчайшие сроки
								</span>
							</div>
						</div>
					</li>
					<li>
						<div class="icon-box">
							<div class="ic-img">
								<img src="icons/manage_accounts_black_48dp.svg" alt="">
							</div>
							<div class="ic-text">
								<span>
									С каждым клиентом работает персональный менеджер, которому можно обратится по любым вопросам и в любое время
								</span>
							</div>
						</div>
					</li>
				</ul>
				<br>
				<p>В нашей компании преимущественно молодой коллектив, но при этом достаточный опытный и компетентный по всем вопросам, который всегда готов прийти на помощь к Вам и Вашей технике. У нас нет «старых» установок и строго установленного графика, все трудятся, доводя каждую сделку до конца, именно поэтому к нам можно обратится в любой день и время.</p>
				<br>
				<p>Так же наши специалисты работают с оригинальными каталогами, по многим маркам техники, поэтому можем поставить запасные части, даже если Вы не знаете их артикул.</p>
				<br>
				<h1>Убедитесь в этом, просто обратившись в нашу компанию.</h1>
				<hr>
				<br>
				<h2>фото отчет с наших складов</h2>
				
			</div>
			<div class="slidebg">
				<div class="sliderBig">
				<div class="sliderBig__item">
					<img data-lazy="img/img-slider/1504143912-35ef0caa47598d431617114f3f689955.jpg"  alt="">
				</div>
				<div class="sliderBig__item">
					<img data-lazy="img/img-slider/774050494_w1024_h1024_774050494.jpg"  alt="">
				</div>
				<div class="sliderBig__item">
					<img data-lazy="img/img-slider/d42b146b83d9ff765087c4c09e952f8d.jpg" alt="">
				</div>
				<div class="sliderBig__item">
					<img data-lazy="img/img-slider/14684165397081.jpg"  alt="">
				</div>
				<div class="sliderBig__item">
					<img data-lazy="img/img-slider/original_7fd9958fcaa55ec5.jpg"  alt="">
				</div>
				<div class="sliderBig__item">
					<img data-lazy="img/img-slider/sklad-1.jpg"  alt="">
				</div>
				<div class="sliderBig__item">
					<img data-lazy="img/img-slider/sklad-2.jpg" alt="">
				</div>
			</div>
			<div class="slider">
				<div class="slider__item">
					<img src="img/img-slider/1504143912-35ef0caa47598d431617114f3f689955.jpg" alt="">
				</div>
				<div class="slider__item">
					<img src="img/img-slider/774050494_w1024_h1024_774050494.jpg" alt="">
				</div>
				<div class="slider__item">
					<img src="img/img-slider/d42b146b83d9ff765087c4c09e952f8d.jpg" alt="">
				</div>
				<div class="slider__item">
					<img src="img/img-slider/14684165397081.jpg" alt="">
				</div>
				<div class="slider__item">
					<img src="img/img-slider/original_7fd9958fcaa55ec5.jpg" alt="">
				</div>
				<div class="slider__item">
					<img src="img/img-slider/sklad-1.jpg" alt="">
				</div>
				<div class="slider__item">
					<img src="img/img-slider/sklad-2.jpg" alt="">
				</div>
			</div>
			</div>
			
			<!-- <div class="zakaz-denali" style="background-image: url(img/background_header.jpg);">
				<div class="container">
					<div class="zakaz-denali-c">
						<div class="zakaz-denali-h"><h2>Заказ детали</h2></div>
						<form action="#" class="form validation" enctype="multipart/form-data" method="POST" novalidate id="mainForm">
							
							<div class="fr-item">
								<label class="input">
									<input type="text" data-parsley-errors-messages-disabled required value name="name" id="name" placeholder="Имя">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="text" value name="company" placeholder="Компания" id="company">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="tel" data-parsley-pattern="\+[0-9]\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}" data-parsley-errors-messages-disabled="" required="" value="" name="phone" placeholder="Телефон" id="phone">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="email" data-parsley-errors-messages-disabled="" required="" value="" name="email" placeholder="E-mail" id="email">
								</label>
							</div>
							<div class="fr-item full-width">
								<label class="input">
									<input type="text" data-parsley-errors-messages-disabled="" required="" value="" name="product" placeholder="Что ищете?" id="product">	
								</label>
							</div>
							<div class="fr-item full-width">
								<label class="textarea">
										<textarea name="comment" placeholder="Комментарий" id="comment"></textarea>
								</label>
							</div> -->
							<!-- <div class="fr-item full-width">
								<div class="fr-title">Прикрепить файл</div>
								<div class="file-wrp">
									<div class="file dz-clickable">
										Выберите 
									</div>
								</div>
							</div> -->
							<!-- <div class="footer-btn">
								<input type="submit" id="sendMail" class="btn btn-style2" value="Оставить заявку" name="button" data-action="p_detail"><div id="errorMess"></div>
							<div class="errorMess" id="errorMess"></div>
							</div>
							
						</form>
					</div>
				</div>
			</div> -->
		</main>
		<?php echo $footer; ?>		
	</div>
<?php echo $fancybox; ?>
<?php echo $scripts ?>
</body>
</html>