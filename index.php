<?php

require_once "db.php";
include('static.php');
$box = $pdo->query("select * from ct_box where id_ct_box <= 4");
$ct_box = $box->fetchAll();
$sub_link = $pdo->query("select * from ct_sub_link where id_ct_box <=4");
$ct_sub_link = $sub_link->fetchAll();
$spare = $pdo->query("select * from spare_part");
$spare_part = $spare->fetchAll();
$spare_receipt = $pdo->query("select * from spare_part order by id_spare_part desc limit 5");
$spare_part_receipt = $spare_receipt->fetchAll();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Грот</title>
	<link rel="stylesheet" href="style.css">
	<?php echo $header_scirpts;?>
</head>
<body style="overflow-y: hidden;">
	<div class="site-wrapper">
		<?php echo $loader;?>
		<?php if(isset($_GET['id_spare_search'])) echo $_GET['id_spare_search'];?>
		<?php echo $header; ?>
		<main class="content">
			<div class="top-screen index-screen">
				<video data-object-fit="cover" playsinline autoplay loop muted>
   					<source src="img/bg.mp4">
  				</video>
  				<div class="container">
  					<h1 class="caption-hidden" hidden="#">Грот</h1>
  					<div class="ts-seatch">
  						<div class="sr-caption">
  							ПОИСК ПО АРТИКУЛУ
  						</div>
  						<form action="/search.php" class="h-search" method="GET">
  							<input type="text" placeholder="Введите номер или название запчасти" name="search" id="elem-search" autocomplete="off">
  							<button id="find-btn" name="find_btn">Найти</button>
  						</form>
  						<div class="container">
  							<ul class="elements-search">
  							<?php foreach($spare_part as $spare_parts): ?>
  								<li value="<?= $spare_parts['id_spare_part']; ?>">
  									<a href="<?= $spare_parts['spare_part_link']; ?>">
  										<p>Наименование запчасти:</p> 
  										<div class="title_spare_name">
  											<?= $spare_parts['spare_part_name']; ?>
  										</div>  
  										<br>
  										<p>Артикул запчасти:</p> 
  										<div class="title_vender">
  											<?= $spare_parts['vender_code']; ?>
  										</div>
  									</a>
  								</li>
  							<?php endforeach; ?>
  							</ul>
  						</div>	
  						<div class="h-sub-search">
  							Введите номер детали или название, например 227-6949
  						</div>

  					</div>

  				</div>
			</div>
			<div class="container">
				<div class="catalog">
					<div class="catalog-h">
						<h2 class="cw">КАТАЛОГ</h2>
						<div class="cw-ot">
							<a href="catalog.php" class="link">ВЕСЬ КАТАЛОГ</a>
						</div>
					</div>
					<ul class="catalog-box-list">
						<?php foreach($ct_box as $ct_boxes): $count = 0;?>
							<li>
								<div class="ct-box">
									<a href="<?= $ct_boxes['ct_img_link']?>" class="ct-img">
										<img class="lazy" data-src="<?= $ct_boxes['ct_img']?>" alt="">
									</a>
									<div class="ct-content">
										
											<div class="ct-content-h">
												<a href="<?= $ct_boxes['ct_caption_link']?>" class="ct-caption"><?= $ct_boxes['ct_caption']?></a>
											</div>
											<div class="ct-content-inner">
												<ul class="ct-content-list">
													<?php $id_ct_box =  $ct_boxes['id_ct_box'];?>
													<?php $sub_link = $pdo->query("select * from ct_sub_link where id_ct_box = '$id_ct_box'");?>
													<?php $ct_sub_link = $sub_link->fetchAll(); ?>
													<?php foreach($ct_sub_link as $ct_sub_links): ?>
														<li  class="ct-content-list__links <?= $ct_sub_links['ct_sub_invise']?>">
															<?php if($ct_sub_links['id_ct_box'] ==$ct_boxes['id_ct_box'] ): $count = $count+1 ?>
																<a  href="<?= $ct_sub_links['sub_link_link']?>">
																	<?= $ct_sub_links['sub_link_text']?>
																</a>
															<?php endif; ?>
														</li>	
													<?php endforeach; ?>
												</ul>
											</div>
										<?php 
											if($count > 3){
											 print '<a href="#" class="link ct-sub-show">Еще разделы</a>'; 
											}
											?>
											
									</div>
								</div>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="receipts">
					<div class="receipts-h">
						<h2>
							Недавнии поступления на складе:
						</h2>
					</div>
					<ul class="receipts-list">
						<?php foreach($spare_part_receipt as $spare_part_receipts): ?>
							<li>
								<a href="/<?= $spare_part_receipts['spare_part_link'] ?>">
									<div class="receipts_img">
										<img class="lazy" data-src="<?= $spare_part_receipts['img_spare_part'] ?>" alt="">
									</div>
									<div class="receipts_name">
										<p>
											<?= $spare_part_receipts['spare_part_name'] ?>
										</p>
									</div>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
			<div class="about" style="background-image: url(img/background_header.jpg); background-repeat: no-repeat; background-position: center; background-size: cover ;background-size: auto;">
				<div class="container">
					<div class="about-c">
						<div class="about-h"><h2>О компании</h2></div>
						<div class="about-main">
							<div class="ab-m-text">«ГРОТ» — Ваш надежный поставщик запасных частей для горной и строительной техники.
								<ul class="ad-m-komp">
									<li class="ad-m-komp__counter">Оперативность поставки в любую точку России</li>
									<li class="ad-m-komp__counter">Молодой коллектив с креативным подходом, «молодым», мышлением, гибкими решениями</li>
									<li class="ad-m-komp__counter">Большое кол-во запчастей, что может обеспечивать комплексное снабжение</li>
									<li class="ad-m-komp__counter">Оперативная отработка запросов</li>
									<li class="ad-m-komp__counter">Оригинальные каталоги с запчастями, возможность подбирать безошибочно запчасти, даже если клиент не знает артикула</li>
								</ul>
							</div>
							<div class="map">
								<iframe class="lazy" data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2187.2150103281297!2d60.53304741587938!3d56.75644538083372!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43c1661a08079079%3A0xa494ecd45d21bec4!2z0JPQsNGA0LDQttC90LDRjyDRg9C7LiwgMjQsINCV0LrQsNGC0LXRgNC40L3QsdGD0YDQsywg0KHQstC10YDQtNC70L7QstGB0LrQsNGPINC-0LHQuy4sIDYyMDAxNg!5e0!3m2!1sru!2sru!4v1632997326894!5m2!1sru!2sru" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
							</div>

						</div>
						<div class="about-zapros"></div>
					</div>
				</div>
			</div>
			
		</main>
		<?php echo $footer; ?>
	</div>
<?php echo $fancybox; ?>
<?php echo $scripts; ?>
</body>
</html>