<?php 
require_once "db.php";
include('static.php');
$spare = $pdo->query("select * from spare_part");
$spare_part = $spare->fetchAll();
$mas_id = [];
if (isset($_GET['search'])){
	if (empty($_GET['search'])){
		$spare_search = $pdo->query("select * from spare_part where id_spare_part = 0");
		$spare_part_search = $spare_search->fetchAll();
		$result_search = 'По данному запросу ничего не найдено';	
		
	}
	else{
		$id_spare_search = $_GET['search'];
		$li  = (info_get_tags($id_spare_search));
		$count = count($li);
		for($i = 0; $i< $count; $i++){
			$or .= "id_spare_part = $li[$i] or ";
		}
		$or = substr($or, 0, -3);	
		$spare_search = $pdo->query("select * from spare_part where $or");
		$spare_part_search = $spare_search->fetchAll();
		$result_search = 'Запчаси по данному запросу:';
	}
	
	foreach($spare_part_search as $spare_part_searchs){
			$id_man = $spare_part_searchs['id_manufacturer'];
			$man = $pdo->query("select * from manufacturer where id_manufacturer = $id_man"); 
			$manufactur = $man->fetchAll();
		}
	
}

function info_get_tags( $arg ) {
    
    $tags_arr = explode( ',' , $arg );
    return $tags_arr;
}
#
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Грот</title>
	<link rel="stylesheet" href="/style.css">
<script type="text/javascript" src="/scripts/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.lazy.min.js"></script>
<script type="text/javascript" src="/scripts/lazy-load.js"></script>

</head>
<body>
	<div class="site-wrapper">
		<?php echo $header; ?>
		<main class="content">
			<div class="top-screen__catalog index-screen__catalog" style="background-image: url(img/search.png); background-position: center; background-size: cover ;">
  				<div class="container">
  					<div class="ts-seatch">
  						<div class="sr-caption">ПОИСК ПО АРТИКУЛУ	
  						</div>
  						<form action="/search.php" class="h-search" method="GET">
  							<input type="text" placeholder="Введите номер или название запчасти" name="search" id="elem-search" autocomplete="off">
  							<button id="find-btn" name="find_btn">Найти</button>
  						</form>

  						<ul class="elements-search">
  							<?php foreach($spare_part as $spare_parts): ?>
  								<li value="<?= $spare_parts['id_spare_part']; ?>">
  									<a href="/<?= $spare_parts["spare_part_link"]; ?>">
  										<p>Наименование запчасти:</p> 
  										<div class="title_spare_name">
  											<?= $spare_parts["spare_part_name"]; ?>
  										</div>  
  										<br>
  										<p>Артикул запчасти:</p> 
  										<div class="title_vender">
  											<?= $spare_parts["vender_code"]; ?>
  										</div>
  									</a>
  								</li>
  							<?php endforeach; ?>
  						</ul>
  						<div class="h-sub-search">
  							Введите номер детали или название, например 227-6949
  						</div>
  						
  					</div>

  					</div>
				</div>

			<div class="container">
				<h1><?php echo $result_search; ?></h1>
				<div class="ctg-content">
					<ul class="ctg-c-list">

						<?php foreach($spare_part_search as $spare_part_searchs): ?>
						<?php foreach($manufactur as $manufactures): ?>

							<li class="lazy" data-loader="examplePlugin">
								<div class="ctg-box">
									<a href="/<?= $spare_part_searchs["spare_part_link"] ?>" class="ctg-img">
										<img class="lazy" data-src="/<?= $spare_part_searchs["img_spare_part"] ?>" alt="">
									</a>
									<div class="ctg-info">
										<ul class="ctg-info-list">
											<li>
												<div class="ctg-caption-list">
													<a href="/<?= $spare_part_searchs["spare_part_link"] ?>"><?= $spare_part_searchs["spare_part_name"] ?>
														
													</a>
												</div>
											</li>
											<li>
												<div class="ctg-i-l-title">
													производитель:
												</div>
												<div class="ctg-i-l-prop">
													<a href="/<?= $manufactures["link_manufacturer"] ?>">
														<?= $manufactures["name_manufacturer"] ?>
													</a>
												</div>
											</li>
											<li>
												<div class="ctg-i-l-title">
													артикул:
												</div>
												<div class="ctg-i-l-vender">
													<?= $spare_part_searchs["vender_code"] ?>
												</div>
											</li>
										</ul>
									</div>
									<div class="ctg-zapros">
										<p>
										Количество запчастей на складе:
										<?= $spare_part_searchs["number"]; ?></p>
										<p>Оставить заявку</p>

										<a  style= "cursor: pointer;" id="order" class="btn order">Заказать деталь</a>
									</div>
								</div>	
							</li>
						<?php endforeach; ?>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</main>
		<?php echo $footer; ?>
	</div>
<?php echo $fancybox; ?>
<?php echo $scripts_search; ?>

</body>
</html>
