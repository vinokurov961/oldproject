<?php
require_once "db.php";
$man = $pdo->query("select * from manufacturer order by name_manufacturer ASC");
$manufactur = $man->fetchAll();
include('static.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Грот</title>
	<link rel="stylesheet" href="style.css">
	<?php echo $header_scirpts;?>
</head>
<body style="overflow-y: hidden;">
	<div class="site-wrapper">
		<?php echo $loader;?>
		<?php echo $header; ?>
		<main class="content">
			<div class="top-screen__catalog index-screen__catalog" style="background-image: url(../../img/manafactur.jpg); background-position: center; background-size: cover ;">
  				<div class="container">
  					<div class="ts-head">
  						<h1>Производители</h1>
  					</div>
  				</div>
			</div>

			<div class="container">
				<h1 class="manafacture_h1"><p>Наша компания предоставляет запчасти производителей представленных ниже</p></h1>
				<div class="manafactur-inner">
					<ul class="man-list">
						<?php foreach($manufactur as $manufactures): ?>
							<li>
								<div class="man-box">
									<a href="<?= $manufactures['link_manufacturer']?>" class="man-img"><img class="lazy" data-src="manufacturer/img/<?= $manufactures['img_manufacturer']?>" alt=""></a>
									<div class="man-name">
										<a href="<?= $manufactures['link_manufacturer']?>" class="link-man-name"><?= $manufactures['name_manufacturer']?></a>
									</div>
								</div>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
				
			</div>
		</main>
		<?php echo $footer; ?>
	</div>
<?php echo $fancybox; ?>
<?php echo $scripts; ?>
</body>
</html>