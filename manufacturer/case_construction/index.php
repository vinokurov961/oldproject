

<?php
require_once "../../db.php";
include("../../static.php");
$man = $pdo->query("select * from manufacturer where id_manufacturer = 3");
$manufactur = $man->fetchAll();
$spare = $pdo->query("select * from spare_part where id_manufacturer = 3");
$spare_part = $spare->fetchAll();

$page = $_GET["page"]+1;
$count = 10;
$page_count = floor(count($spare_part)/$count);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Грот</title>
	<link rel="stylesheet" href="/style.css">
	<?php echo $header_scirpts;?>
</head>
<body style="overflow-y: hidden;">
	<div class="site-wrapper">
		<?php echo $loader;?>
		<?php echo $header; ?>
		<main class="content">
			<div class="top-screen__catalog index-screen__catalog" style="background-image: url(/img/manafactur.jpg); background-position: center; background-size: cover ;background-size: auto;">
  				<div class="container">
  					<div class="ts-head">
  						<h1>
  							<?php foreach($manufactur as $manufactures): ?>
  								<?= $manufactures["name_manufacturer"] ?>
  							<?php endforeach; ?>
  						</h1>
  					</div>
  				</div>
			</div>

			<div class="container">
				<div class="ctg-content">
					<ul class="ctg-c-list">
						<?php for($i = ($page-1)*$count; $i < ($page)*$count; $i++): ?>
						<?php foreach($spare_part as $key => $spare_parts): ?>
						<?php if($i==$key): ?>
						<?php foreach($manufactur as $manufactures): ?>
							<li class="lazy" data-loader="examplePlugin">
								<div class="ctg-box">
									<a href="/<?= $spare_parts["spare_part_link"] ?>" class="ctg-img">
										<img class="lazy" data-src="/<?= $spare_parts["img_spare_part"] ?>" alt="">
									</a>
									<div class="ctg-info">
										<ul class="ctg-info-list">
											<li>
												<div class="ctg-caption-list">
													<a href="/<?= $spare_parts["spare_part_link"] ?>"><?= $spare_parts["spare_part_name"] ?>
														
													</a>
												</div>
											</li>
											<li>
												<div class="ctg-i-l-title">
													производитель:
												</div>
												<div class="ctg-i-l-prop">
													<a href="/<?= $manufactures["link_manufacturer"] ?>">
														<?= $manufactures["name_manufacturer"] ?>
													</a>
												</div>
											</li>
											<li>
												<div class="ctg-i-l-title">
													артикул:
												</div>
												<div class="ctg-i-l-vender">
													<?= $spare_parts["vender_code"] ?>
												</div>
											</li>
										</ul>
									</div>
									<div class="ctg-zapros">
										<p>
										Количество запчастей на складе:
										<?= $spare_parts["number"]; ?></p>
										<p>Оставить заявку</p>

										<a  style= "cursor: pointer;" id="order" class="btn order">Заказать деталь</a>
									</div>
								</div>	
							</li>
						<?php endforeach; ?>
						<?php endif; ?>
						<?php endforeach; ?>
						<?php endfor; ?>
					</ul>
					<div class="page_list">
						<a href="?page=<?php echo $page-2; ?>" class="forward_page <?php if($page == 1 ): ?> hide <?php endif; ?>"><button>Предыдущая страница</button></a>
						<ul>
						<?php for($p=0; $p<= $page_count; $p++): ?>

							<li value ="<?= $p+1 ?>" class="page_number">
								<a href="?page=<?php echo $p; ?>" <?php if($p == $page-1): ?> class = "active_page" <?php endif; ?> >
								<button class="page_button">
									<?= $p+1 ?>
								</button>
							</a>
							</li>
						<?php endfor; ?>
						</ul>

						<a href="?page=<?php echo $page; ?>" class="next_page <?php if($page == $page_count+1 ): ?> hide <?php endif; ?>"><button>Следующая страница</button></a>
					</div>
				</div>
			</div>
		</main>

			<?php echo $footer; ?>
	</div>
<?php echo $fancybox; ?>
<?php echo $scripts; ?>

</body>
</html>
