<?php

require_once "db.php";
$box = $pdo->query("select * from ct_box");
$ct_box = $box->fetchAll();
$spare = $pdo->query("select * from spare_part");
$spare_part = $spare->fetchAll();
include('static.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Грот</title>
	<link rel="stylesheet" href="style.css">
	<?php echo $header_scirpts;?>
</head>
<body style="overflow-y: hidden;">
	<div class="site-wrapper">
		<?php echo $loader;?>
		<?php echo $header; ?>
		<main class="content">
			<div class="top-screen__catalog index-screen__catalog" style="background-image: url(img/background_catalog.jpg);background-position: center; background-size: cover ;">
  				<div class="container">
  					<div class="ts-seatch">
  						<div class="sr-caption">ПОИСК ПО АРТИКУЛУ	
  						</div>
  						<form action="/search.php" class="h-search" method="GET">
  							<input type="text" placeholder="Введите номер или название запчасти" name="search" id="elem-search" autocomplete="off">
  							<button id="find-btn" name="find_btn">Найти</button>
  						</form>

  						<ul class="elements-search">
  							<?php foreach($spare_part as $spare_parts): ?>
  								<li value="<?= $spare_parts["id_spare_part"]; ?>">
  									<a href="<?= $spare_parts['spare_part_link']; ?>">
  										<p>Наименование запчасти:</p> 
  										<div class="title_spare_name">
  											<?= $spare_parts['spare_part_name']; ?>
  										</div>  
  										<br>
  										<p>Артикул запчасти:</p> 
  										<div class="title_vender">
  											<?= $spare_parts['vender_code']; ?>
  										</div>
  									</a>
  								</li>
  							<?php endforeach; ?>
  						</ul>

  						<div class="h-sub-search">
  							Введите номер детали или название, например 227-6949
  						</div>

  					</div>

  					</div>
				</div>
			<div class="main-catalog">
				<div class="container">
					<div class="main-cols">
						<div class="aside">
							<div class="sled">
								<div class="as-box">
									<ul class="as-list">
										<?php foreach($ct_box as $ct_boxes): ?>
												<li class="subnav">
													
													<a href="<?=$ct_boxes['ct_caption_link'] ?>" class="op-active"><?= $ct_boxes['ct_caption'] ?></a>
													<i class="sub-arrow-r">
														<span class="s-arrow-l"></span>
														<span class="s-arrow-r"></span>
													</i>
													
													<div class="subnav-wrp" style="display: none;">
														<ul class="subnav-list">
															<li >
																<?php $id_ct_box =  $ct_boxes['id_ct_box'];?>
																<?php $sub_link = $pdo->query("select * from ct_sub_link where id_ct_box = '$id_ct_box'");?>
																<?php $ct_sub_link = $sub_link->fetchAll(); ?>
																<?php foreach($ct_sub_link as $ct_sub_links): ?>
																	
																		<a href="<?= $ct_sub_links['sub_link_link'] ?>"><?= $ct_sub_links['sub_link_text']?>
																		</a>

																<?php endforeach; ?>
															</li>
														</ul>
													</div>
													
												</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="mc-content">
							<h1>Каталог</h1>
							<ul class="ct-list">
							<?php foreach($ct_box as $ct_boxes): $count = 0?>
								<li class="lazy" data-loader="examplePlugin">
									
									<div class="ct-box">
										<a href="<?= $ct_boxes['ct_img_link']?>" class="ct-img">
											<img class="lazy" data-src="<?= $ct_boxes['ct_img']?>" alt="">
										</a>
										<div class="ct-content">
											<div class="ct-top">
												<a href="<?= $ct_boxes['ct_caption_link']?>" class="ct-caption"><?= $ct_boxes['ct_caption']?></a>
												<ul class="ct-sub-link">
													<?php $id_ct_box =  $ct_boxes['id_ct_box'];?>
													<?php $sub_link = $pdo->query("select * from ct_sub_link where id_ct_box = '$id_ct_box'");?>
													<?php $ct_sub_link = $sub_link->fetchAll(); ?>
													<?php foreach($ct_sub_link as $ct_sub_links): ?>
														<li  class="ct-content-list__links <?= $ct_sub_links['ct_sub_invise']?>">
															<?php if($ct_sub_links['id_ct_box'] ==$ct_boxes['id_ct_box'] ): $count = $count+1 ?>
																<a  href="<?= $ct_sub_links['sub_link_link']?>">
																	<?= $ct_sub_links['sub_link_text']?>
																</a>
															<?php endif; ?>
														</li>	
													<?php endforeach; ?>
												</ul>
											</div>
											<?php 
											if($count > 5){
											 print '<a href="#" class="link ct-sub-show">Еще разделы</a>'; 
											}
											?>
											
										</div>
									</div>
								</li>
								<?php endforeach; ?>
							</ul>

						</div>
					</div>
				</div>
			</div>
			
			<!-- <div class="zakaz-denali" style="background-image: url(img/background_header.jpg);">
				<div class="container">
					<div class="zakaz-denali-c">
						<div class="zakaz-denali-h"><h2>Заказ детали</h2></div>
						<form action="#" class="form validation" enctype="multipart/form-data" method="POST" novalidate id="mainForm">
							
							<div class="fr-item">
								<label class="input">
									<input type="text" data-parsley-errors-messages-disabled required value name="name" id="name" placeholder="Имя">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="text" value name="company" placeholder="Компания" id="company">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="tel" data-parsley-pattern="\+[0-9]\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}" data-parsley-errors-messages-disabled="" required="" value="" name="phone" placeholder="Телефон" id="phone">
								</label>
							</div>
							<div class="fr-item">
								<label class="input">
									<input type="email" data-parsley-errors-messages-disabled="" required="" value="" name="email" placeholder="E-mail" id="email">
								</label>
							</div>
							<div class="fr-item full-width">
								<label class="input">
									<input type="text" data-parsley-errors-messages-disabled="" required="" value="" name="product" placeholder="Что ищете?" id="product">	
								</label>
							</div>
							<div class="fr-item full-width">
								<label class="textarea">
										<textarea name="comment" placeholder="Комментарий" id="comment"></textarea>
								</label>
							</div> -->
							<!-- <div class="fr-item full-width">
								<div class="fr-title">Прикрепить файл</div>
								<div class="file-wrp">
									<div class="file dz-clickable">
										Выберите 
									</div>
								</div>
							</div> -->
							<!-- <div class="footer-btn">
								<input type="submit" id="sendMail" class="btn btn-style2" value="Оставить заявку" name="button" data-action="p_detail"><div id="errorMess"></div>
							<div class="errorMess" id="errorMess"></div>
							</div>
							
						</zz>
					</div>
				</div>
			</div> -->
		</main>
		<?php echo $footer; ?>
	</div>
<?php echo $fancybox; ?>
<?php echo $scripts; ?>
</body>
</html>